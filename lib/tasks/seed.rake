namespace :seed do
  desc "TODO"
  task create_some_users: :environment do
    brands = ['O2', 'Vodafone', 'Telekom']
    100.times do
      gender = (rand(100) % 2 == 0)? 'male' : 'female'
      user = {email: Faker::Internet.email }
      user.merge!({gender: gender})
      if(gender == 'male')
        user.merge!({first_name: Faker::Name.male_first_name})
      else
        user.merge!({first_name: Faker::Name.female_first_name})
      end
      user.merge!({last_name: Faker::Name.last_name})
      user.merge!({company: Faker::Company.name})
      user.merge!({brand: brands[rand(3)]})
      user.merge!({password: Faker::Internet.password})
      User.create!(user)
    end
  end

  task create_some_documents: :environment do
    doc_types = ['Invoice', 'Reminder', 'Offer']
    User.all.each do |u|
      (rand(3) + 1).times do
        archive_item = ArchiveItem.new
        archive_item.document_type = doc_types[rand(3)]
        archive_item.user = u
        archive_document = ArchiveDocument.new
        archive_document.file_name = u.last_name + archive_item.document_type + ".pdf"
        archive_document.binary = "some file goes here"
        archive_item.ArchiveDocument << archive_document
        archive_item.save!
      end
    end
  end
end
