class ArchiveDocument
  include Mongoid::Document
  include Mongoid::Timestamps

  embedded_in :ArchiveItem

  field :file_name, type: String
  field :mime_type, type: String
  field :gridfs_id, type: BSON::ObjectId

  def binary=(document)
    io = StringIO.new()
    io.puts document
    io.rewind
    self.gridfs_id = Mongoid::Clients.default.database.fs.upload_from_stream(self.file_name, io)
  end

  def binary
    data = StringIO.new
    Mongoid::Clients.default.database.fs.download_to_stream(self.gridfs_id, data)
    data.string
  end
end
