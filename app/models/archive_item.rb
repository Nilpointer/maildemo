class ArchiveItem
  include Mongoid::Document
  include Mongoid::Timestamps

  belongs_to :user
  embeds_many :ArchiveDocument

  field :document_type, type: String

end
