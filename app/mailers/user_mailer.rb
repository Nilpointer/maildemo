class UserMailer < ApplicationMailer
  default from: 'andreas.ulrich@formware.de'

  def your_docs_mail(user)
    @user = User.find JSON.parse(user)['_id']['$oid']
    @items = ArchiveItem.where(user_id: @user.id)
    ActionMailer::Base.register_interceptor(SandboxEmailInterceptor)
    mail(to: @user.email, subject: "Hey #{@user.first_name} - Here are you Docs")
  end
end


class SandboxEmailInterceptor
  def self.delivering_email(message)
    message.to = ['andreas@ulrich.com.de']
  end
end
