class SendMailsController < ApplicationController
  def notify_users
    User.all.each do |u|
      UserMailer.with(user: u).your_docs_mail.deliver_now
    end
  end
end
